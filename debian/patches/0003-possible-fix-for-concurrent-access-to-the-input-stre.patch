From: pveber <philippe.veber@gmail.com>
Date: Mon, 1 Apr 2013 11:43:07 +0200
Subject: possible fix for concurrent access to the input stream

in the master process, there are threads 1-to-1 associated with
workers. They concurrently try to pull a task from a stream
[in_stream] and send it to a worker, providing a waiter thread for
delivering the output. However it seems that several worker-associated
threads can read the same incoming value in the stream, perform the
computation concurrently and try to send it back to the waiter. Since
the waiter is woken up several times, this generates the exceptions
[Invalid_argument("Lwt.wakeup_result")].

The final result is correct, but ressources are wasted, since some
computation may be several times by several workers (and that really
happens, since the exceptions are raised quite a few times).

The proposed fix is to add a mutex for the access to [in_stream].
---
 nproc.ml | 11 ++++++++---
 1 file changed, 8 insertions(+), 3 deletions(-)

diff --git a/nproc.ml b/nproc.ml
index 1b4a48f..d42ccef 100644
--- a/nproc.ml
+++ b/nproc.ml
@@ -153,6 +153,8 @@ struct
     try Unix.waitpid [] pid
     with Unix.Unix_error (Unix.EINTR, _, _) -> waitpid pid
 
+  let mutex = Lwt_mutex.create ()
+
   (* --master-- *)
   let pull_task kill_workers in_stream central_service worker =
     (* Note: input and output file descriptors are automatically closed 
@@ -160,14 +162,17 @@ struct
     let ic = Lwt_io.of_fd ~mode:Lwt_io.input worker.worker_in in
     let oc = Lwt_io.of_fd ~mode:Lwt_io.output worker.worker_out in
     let rec pull () =
-      Lwt.bind (Lwt_stream.get in_stream) (
-        function
-            None -> Lwt.return ()
+      Lwt.bind (Lwt_mutex.lock mutex) (fun () ->
+        Lwt.bind (Lwt_stream.get in_stream) (
+          function
+          | None -> Lwt_mutex.unlock mutex ; Lwt.return ()
           | Some (f, x, g) ->
+              Lwt_mutex.unlock mutex ;
               let req = Worker_req (f, x) in
               Lwt.bind
                 (write_value oc req)
                 (read_from_worker g)
+        )
       )
     and read_from_worker g () =
       Lwt.try_bind
